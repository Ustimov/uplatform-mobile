﻿using System;

namespace UPlatform
{
    public enum PageEntity
    {
        CART,
        CONTACTS,
        ABOUT_US,
        ABOUT_PROGRAM,
        SETTINGS
    }
}

