﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using System.Threading.Tasks;
using System.Diagnostics;

using UPlatform.Resources;

using Xamarin.Forms;

namespace UPlatform
{
    public class CustomDialog : StackLayout
    {
        public event EventHandler<EventArgs> OnShow;

        public CustomDialog()
        {
            this.HeightRequest = 200;
        }

        public void Show()
        {
            if (OnShow != null)
            {
                OnShow(this, null);
            }
        }
    }

    public partial class CheckoutPage : ContentPage
    {
        public CheckoutPage()
        {
            InitializeComponent();
        }

        public async void OnButtonClicked(object sender, EventArgs e)
        {
            Debug.WriteLine(name.Text);
            Debug.WriteLine(address.Text);
            Debug.WriteLine(phone.Text);

            if (name.Text == null || name.Text == String.Empty ||
                address.Text == null || address.Text == String.Empty || 
                phone.Text == null || phone.Text == String.Empty)
            {
                await DisplayAlert(AppResources.Error,
                    AppResources.FillAllFields, AppResources.OK);
                //customDialog = new CustomDialog();

                //DependencyService.Get<IDisplayAlertService>().ShowAlert(AppResources.Error,
                //    AppResources.FillAllFields, AppResources.OK);

                return;
            }

            /*
            var orderResult = false;

            // TODO: Action in case of failing order.
            await Task.Run(() =>
            {
                orderResult = CartDataModel.Checkout(name.Text, address.Text, phone.Text);
            });

            if (orderResult == true)
            {
                await DisplayAlert("Order received!", "Thanks! Your order has been received.", "OK");
                CartDataModel.Items.Clear();
            }
            else
            {
                await DisplayAlert("Error", "Order has not be received.", "OK");
            }

            await Navigation.PopToRootAsync();
            */

            await Navigation.PushModalAsync(new LoadingPage());

            await Task.Run(() =>
                {
                    var result = CartDataModel.Checkout(name.Text, address.Text, phone.Text);

                    if (result)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                            {
                                await DisplayAlert(AppResources.Success,
                                    AppResources.OrderReceived, AppResources.OK);
                                await Navigation.PopModalAsync();
                                await Navigation.PopToRootAsync();
                            });
                        CartDataModel.Items.Clear();
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                            {
                                await DisplayAlert(AppResources.Error,
                                    AppResources.OrderNotReceived, AppResources.OK);
                                await Navigation.PopModalAsync();
                            });
                    }
                });
        }
    }
}
