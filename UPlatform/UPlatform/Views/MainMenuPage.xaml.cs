﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using Xamarin.Forms;

namespace UPlatform
{
    public partial class MainMenuPage : MasterDetailPage
    {
        //private string previousPageName;

        public MainMenuPage()
        {
            InitializeComponent();

            //this.ToolbarItems.Add(new ToolbarItem("add", "monkeyicon.png", () => { }));



            this.BindingContext = MainMenuItemDataModel.Demo();

            //var t = Navigation.PushModalAsync(new LoadPage());
            //t.Wait();

            //((MenuPage)Detail).OnMenuPageStatusChanged += OnMenuPageStatusChanged;
        }

        /*
        void OnMenuPageStatusChanged (object sender, MenuPageStatusChangedEventArgs e)
        {
            var t = Navigation.PopModalAsync();
            t.Wait();
        }
        */
         
        public void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListView listView = (ListView)sender;
            
            if (listView.SelectedItem == null)
            {
                // Setting selected item to null does fires this event again.
                return; 
            }           
            else
            {
                // Overwise, next click to this item doesn't fire event.
                listView.SelectedItem = null;
            }

            var mainMenuItemDataModel = (MainMenuItemDataModel)listView.SelectedItem;
            ProcessNavigation(mainMenuItemDataModel.PageEntity);
            //Navigation.PushAsync(new ProductDetailPage(cartItemDataModel));
        }

        private async void ProcessNavigation(PageEntity pageEntity)
        {
            switch(pageEntity)
            {
                case PageEntity.ABOUT_US:
                    //MenuDataModel.Update();
                    IsPresented = false;
                    await Navigation.PushAsync(new AboutUsPage());
                    break;
                case PageEntity.CONTACTS:
                    IsPresented = false;
                    await Navigation.PushAsync(new ContactsPage());
                    break;
                case PageEntity.CART:
                    IsPresented = false;
                    await Navigation.PushAsync(new CartPage());
                    break;
                case PageEntity.ABOUT_PROGRAM:
                    IsPresented = false;
                    await Navigation.PushAsync(new AboutProgramPage());
                    break;
            }
        }
    }
}
