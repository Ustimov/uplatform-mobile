﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using Xamarin.Forms;

namespace UPlatform
{
    public partial class CartPage
    {
        public CartPage()
        {
            InitializeComponent();

            this.BindingContext = new CartDataModel();

            listView.ItemsSource = CartDataModel.Items;

            this.Appearing += CartPage_Appearing;
        }

        void CartPage_Appearing(object sender, System.EventArgs e)
        {
            // Crutch for sum updating.
            var cartDataModel = (CartDataModel)this.BindingContext;
            cartDataModel.Sum = 0;

            if (CartDataModel.IsEmpty())
            {
                button.IsEnabled = false;
            }
        }

        public async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListView listView = (ListView)sender;

            if (listView.SelectedItem == null)
            {
                // Setting selected item to null is fires this event again.
                return;
            }
            else
            {
                // Overwise, next click to this item doesn't fire event.
                listView.SelectedItem = null;
            }

            var cartItemDataModel = (CartItemDataModel)listView.SelectedItem;
            await Navigation.PushAsync(new ProductDetailPage(cartItemDataModel, true));
        }

        public async void OnButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CheckoutPage());
        }
    }
}
