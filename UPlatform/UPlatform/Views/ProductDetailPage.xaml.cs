﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;

using Xamarin.Forms;
//using System.Diagnostics;

using ImageCircle.Forms.Plugin.Abstractions;

using UPlatform.Resources;

namespace UPlatform
{
    public partial class ProductDetailPage : ContentPage
    {
        private bool isInCart;

        public ProductDetailPage()
        {
            InitializeComponent();
        }

        public ProductDetailPage(CartItemDataModel cartItemDataModel, bool isInCart)
        {
            InitializeComponent();

            this.isInCart = isInCart;

            if (isInCart)
            {
                //var l = new ListView();
                //l.He
                button.Text = AppResources.RemoveFromCart;
            }
            else
            {
                if (CartDataModel.Contains(cartItemDataModel))
                {
                    // Stepper remains enabled in Windows Phone.
                    stepper.IsEnabled = false;

                    button.IsEnabled = false;
                    button.Text = AppResources.AddedToCart;
                }
            }

            this.BindingContext = cartItemDataModel;

        }

        public async void OnButtonClick(object sender, EventArgs e)
        {
            var item = (CartItemDataModel)this.BindingContext;

            if (!isInCart)
            {
                if (!CartDataModel.Contains(item))
                {
                    CartDataModel.Items.Add(item);
                }

                await Navigation.PopAsync();
            }
            else
            {
                CartDataModel.Remove(item);

                await Navigation.PopAsync();
            }
        }
        /*
        protected override void OnAppearing()
        {

            
            base.OnAppearing();
        }
        */
    }
}
