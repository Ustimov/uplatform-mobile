﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

using UPlatform.Resources;

namespace UPlatform
{
    public delegate void MenuPageStatusChanged(object sender, MenuPageStatusChangedEventArgs e);

    public enum MenuPageStatus
    {
        Loading,
        Loaded,
        NotLoaded
    }

    public class MenuPageStatusChangedEventArgs : EventArgs
    {
        public MenuPageStatus Status { get; set; }
    }

    public partial class MenuPage : TabbedPage
    {
        private Dictionary<string, Tuple<ActivityIndicator, ListView>> courseToPageMap = 
            new Dictionary<string, Tuple<ActivityIndicator, ListView>>();

        private MenuPageStatus status = MenuPageStatus.Loading;

        public event MenuPageStatusChanged OnMenuPageStatusChanged;

        public MenuPageStatus Status
        {
            get { return status; }

            set
            {
                status = value;

                if (OnMenuPageStatusChanged != null)
                {
                    OnMenuPageStatusChanged(this, new MenuPageStatusChangedEventArgs { Status = this.Status });
                }
            }     
        }

        public MenuPage()
        {
            InitializeComponent();
            
            ToolbarItems.Add(new ToolbarItem());
//#if ANDROID
            this.CurrentPageChanged += OnCurrentPageChanged;
//#endif

            var menuDataModel = new MenuDataModel();

            this.ItemsSource = menuDataModel.Courses;// MenuDataModel.Demo();//

            Task.Run(async () =>
                {
                    await Navigation.PushModalAsync(new LoadingPage());

                    var result = menuDataModel.LoadCourses();

                    if (result)
                    {
                        Status = MenuPageStatus.Loaded;
                        Device.BeginInvokeOnMainThread(async () =>
                            {
                                await Navigation.PopModalAsync();
                            });
                    }
                    else
                    {
                        Status = MenuPageStatus.NotLoaded;
                        Device.BeginInvokeOnMainThread(async () =>
                            {
                                /*
                                foreach (var item in MenuDataModel.Demo())
                                {
                                    ((ObservableCollection<CourseDataModel>)this.ItemsSource).Add(item);
                                }
                                */

                                await Navigation.PopModalAsync();
                                
                                await DisplayAlert(AppResources.Error,
                                    AppResources.MenuLoadingError, AppResources.OK);
                            });
                    }
                });

//#if ANDROID
            // TODO: Make it better (for Android).
            this.CurrentPageChanged += (object sender, EventArgs e) => 
                {
                    var stackLayout = (StackLayout)((ContentPage)this.CurrentPage).Content;

                    var listView = (ListView)stackLayout.Children[0];

                    if (((ObservableCollection<ProductDataModel>)listView.ItemsSource).Count != 0)
                    {
                        listView.SelectedItem = ((ObservableCollection<ProductDataModel>)listView.ItemsSource)[0];
                        listView.SelectedItem = null;
                    }
                };
//#endif
            /*
            Device.BeginInvokeOnMainThread(() =>
                {
                    menuDataModel.LoadCourses();
                });

            */
            //Navigation.PushModalAsync(new LoadPage());

            /*
            Task.Run(() =>
                {
                    var t = Task.Delay(2000);
                    t.Wait();
                    ((ObservableCollection<CourseDataModel>)this.ItemsSource)
                        .Add(MockClient.GetCourceDataModel("Шашлычок"));
                   
                    ((ObservableCollection<CourseDataModel>)this.ItemsSource)
                        .Add(MockClient.GetCourceDataModel("Пицца"));
                });
            */
        }

        public async void OnItemSelected(object sender, ItemTappedEventArgs e)
        {
            ListView listView = (ListView)sender;

            if (listView.SelectedItem == null)
            {
                // Setting selected item to null is fires this event again.
                return; 
            }           
            else
            {
                // Overwise, next click to this item doesn't fire event.
                listView.SelectedItem = null;
            }

            var cartItemDataModel = new CartItemDataModel((ProductDataModel)e.Item);
            await Navigation.PushAsync(new ProductDetailPage(cartItemDataModel, false));
        }

        public void OnCurrentPageChanged(object sender, EventArgs e)
        {
            var courseDataModel = (CourseDataModel)this.SelectedItem;

            if (courseDataModel.Products.Count == 0 && !courseToPageMap.ContainsKey(courseDataModel.Name))
            {
                var stackLayout = (StackLayout)((ContentPage)this.CurrentPage).Content;
                stackLayout.VerticalOptions = LayoutOptions.Center;

                var listView = (ListView)stackLayout.Children[0];
                listView.IsVisible = false;

                var activityIndicator = (ActivityIndicator)stackLayout.Children[1];
                activityIndicator.IsVisible = true;

                //stackLayout.Children.Add(activityIndicator);

                courseToPageMap.Add(courseDataModel.Name,
                    new Tuple<ActivityIndicator, ListView>(activityIndicator, listView));
                
                courseDataModel.OnProductsLoaded += OnProductsLoaded;

                Task.Run(() =>
                    {
                        var result = courseDataModel.LoadProducts();
                        
                        var tuple = courseToPageMap[courseDataModel.Name];

                        if (result)
                        {
                            
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                tuple.Item2.IsVisible = true;
                                tuple.Item1.IsVisible = false;
                                ((StackLayout)tuple.Item1.Parent).VerticalOptions = LayoutOptions.Start;
                            });
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                                {
                                    tuple.Item1.IsVisible = false;
                                    ((StackLayout)tuple.Item1.Parent).VerticalOptions = LayoutOptions.Start;
                                    await DisplayAlert(AppResources.Error,
                                        AppResources.ProductLoadingError, AppResources.OK);
                                });
                        }

                        courseToPageMap.Remove(courseDataModel.Name);
                    });
            }
        }
            
        public void OnProductsLoaded(object sender, EventArgs e)
        {
            var tuple = courseToPageMap[((CourseDataModel)sender).Name];
            Device.BeginInvokeOnMainThread(() =>
            {
                tuple.Item2.IsVisible = true;
                tuple.Item1.IsVisible = false;
            });
        }



        /*
        protected override void OnAppearing()
        {
            base.OnAppearing();


            var navigationPage = (NavigationPage)App.Current.MainPage;
            navigationPage.BarBackgroundColor = Color.FromHex("#333");


            var stackLayout = (StackLayout)((ContentPage)this.CurrentPage).Content;

            var listView = (ListView)stackLayout.Children[0];

            listView.Focus();

        }
        */
    }
}
