﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace UPlatform
{
	public class App : Application // superclass new in 1.3
	{
		public App ()
		{
            MainPage = new NavigationPage(new MainMenuPage());// { BarTextColor = Color.White };

            /*
            return;

            var loadPage = new LoadPage();
            loadPage.MenuPage.OnMenuPageStatusChanged += OnMenuPageStatusChanged;
            MainPage = new NavigationPage(loadPage);

            Task.Run(() =>
                {
                    var t = Task.Delay(3000);

                    t.Wait();

                    OnMenuPageStatusChanged(new MainMenuPage(),
                        new MenuPageStatusChangedEventArgs { Status = MenuPageStatus.Loaded });
                });
            */
        }

        /*
        private void OnMenuPageStatusChanged(object sender, MenuPageStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case MenuPageStatus.Loaded:
                    MainPage = new NavigationPage((MainMenuPage)sender);
                    break;
                case MenuPageStatus.NotLoaded:
                    // TODO something.
                    break;
            }
        }
        */
    }
}
