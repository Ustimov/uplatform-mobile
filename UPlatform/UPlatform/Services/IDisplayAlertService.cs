﻿using System;

namespace UPlatform
{
    public interface IDisplayAlertService
    {
        void ShowAlert(string title, string content, string buttonText);
    }
}

