﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using System.Threading.Tasks;
using System.Net.Http;

namespace UPlatform
{
    public class Client
    {
        public Client()
        {
        }

        public async static Task<string> Get(string url, int timeout = 100)
        {
            url = String.Format("http://192.168.43.35:8000/{0}", url);
            
            using (var httpClient = new HttpClient())
            {
                httpClient.Timeout = TimeSpan.FromSeconds(timeout);

                try
                {
                    var response = await httpClient.GetAsync(url);

                    // TODO: Try-catch block is needed.
                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    return await Task.Run(() => content);
                }
                catch (Exception e)
                {
                    throw e;    
                }
            }
        }
    }
}

