﻿using System.Globalization;

namespace UPlatform
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }
}
