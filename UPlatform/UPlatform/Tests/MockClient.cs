﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace UPlatform
{
    public class MockClient
    {
        public MockClient()
        {

        }

        public static CourseDataModel GetCourceDataModel(string name/*, int productQuantity*/)
        {
            var courseDataModel = new CourseDataModel
                {
                    Name = name,
                    Products = new ObservableCollection<ProductDataModel>()
                };
            /*
            Task.Run(() =>
                {
                    var t = Task.Delay(3000);

                    t.Wait();

                    for (int i = 0; i < productQuantity; i++)
                    {
                        courseDataModel.Products.Add(new ProductDataModel
                            {
                                Name = "Test product " + i.ToString(),
                                Cost = i
                            });
                    }
                });
            */

            return courseDataModel;
        }

        public static ProductDataModel GetProductDataModel(string name)
        {
            return new ProductDataModel
            {
                Name = name
            };
        }
    }
}

