﻿using System;

//using UPlatform.Resouces;
using UPlatform.Resources;

namespace UPlatform
{
    public class ProductDataModel
    {
        private string image;

        public string Pk { get; set; }

        public string Name { get; set; }

        //TODO: Change to integer.
        public float Cost { get; set; }

        public string Description { get; set; }

        public string Image
        {
            get
            {
                return String.Format("http://192.168.43.35:8000/media/{0}", image);
            }
            set { image = value; }
        }

        public string ImageNative { get { return image; } }

        public string Detail { get { return String.Format(AppResources.ProductDetail, Cost); }}
    }
}

