﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;

namespace UPlatform
{
    public delegate void ProductsLoaded(object sender, EventArgs e);

    public class CourseDataModel
    {
        public event ProductsLoaded OnProductsLoaded;

        public string Pk { get; set; }

        public string Name { get; set; }

        public IList<ProductDataModel> Products { get; set; }

        public CourseDataModel()
        {
            Products = new ObservableCollection<ProductDataModel>();
        }

        public void LoadProductsTest()
        {
            Task.Run(() =>
                {
                    var t = Task.Delay(1000);

                    t.Wait();

                    for (int i = 0; i < 3; i++)
                    {
                        Products.Add(MockClient.GetProductDataModel(i.ToString()));
                    }

                    if (OnProductsLoaded != null)
                    {
                        OnProductsLoaded(this, new EventArgs());
                    }
                });
        }

        public bool LoadProducts()
        {
            try
            {
                var request = Client.Get(String.Format("api/products/{0}/", Pk), 15);
                request.Wait();

                var response = request.Result;

                ObservableCollection<ProductDataModel> products = new ObservableCollection<ProductDataModel>();

                products = JsonConvert.DeserializeObject<ObservableCollection<ProductDataModel>>(response);

                foreach (var item in products)
                {
                    Products.Add(item);
                }

                return true;

                /*
                if (OnProductsLoaded != null)
                {
                    OnProductsLoaded(this, new EventArgs());
                }
                */
            }
            catch (Exception e)
            {
                Debug.WriteLine("Load products error");
                return false;
            }
        }
    }
}

