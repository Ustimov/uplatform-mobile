﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;

namespace UPlatform
{
    public class MenuDataModel
    {
        public MenuDataModel()
        {
            Courses = new ObservableCollection<CourseDataModel>(); 
        }

        public static ObservableCollection<CourseDataModel> Demo()
        {
            return new ObservableCollection<CourseDataModel>
            {
                new CourseDataModel
                {
                    Name = "Pizza",
                    Products = new ObservableCollection<ProductDataModel>
                    {
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "Mario",
                            Cost = 400,
                            Description = "Common pizza. With a very big description. This pizza contains onion, " +
                            "pepper, cheese, meat and so on. We'll be glad if you buy it. Thank you a lot for " +
                            "reading this text. Good bye, dear friend!"
                        },
                        new ProductDataModel
                        {
                            Pk = "2",
                            Name = "European",
                            Cost = 300,
                            Description = "Yet another pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "Mario",
                            Cost = 400,
                            Description = "Common pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "European",
                            Cost = 300,
                            Description = "Yet another pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "Mario",
                            Cost = 400,
                            Description = "Common pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "European",
                            Cost = 300,
                            Description = "Yet another pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "Mario",
                            Cost = 400,
                            Description = "Common pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "European",
                            Cost = 300,
                            Description = "Yet another pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "Mario",
                            Cost = 400,
                            Description = "Common pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "European",
                            Cost = 300,
                            Description = "Yet another pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "Mario",
                            Cost = 400,
                            Description = "Common pizza"
                        },
                        new ProductDataModel
                        {
                            Pk = "1",
                            Name = "European",
                            Cost = 300,
                            Description = "Yet another pizza"
                        }
                    }
                },
                new CourseDataModel
                {
                    Name = "Sushi",
                    Products = new ObservableCollection<ProductDataModel>
                    {
                        new ProductDataModel
                        {
                            Pk = "3",
                            Name = "California",
                            Cost = 200,
                            Description = "Popular sushi"
                        }
                    }
                }
            };
        }

        public IList<CourseDataModel> Courses { get; set; }

        public bool LoadCourses()
        {
            try
            {
                var request = Client.Get("api/category/", 15);
                request.Wait();

                var response = request.Result;

                var courses = JsonConvert.DeserializeObject<ObservableCollection<CourseDataModel>>(response);

                foreach (var item in courses)
                {
                    Courses.Add(item);
                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception in load curses method");
                return false;
            }
        }
    }
}
