﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using UPlatform.Resources;

namespace UPlatform
{
    public class CartDataModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int sum;

        private static ObservableCollection<CartItemDataModel> items
            = new ObservableCollection<CartItemDataModel>();

        public static ObservableCollection<CartItemDataModel> Items
        {
            get { return items; }
            set { items = value; }
        }

        public static bool Contains(CartItemDataModel item)
        {
            return Items.Any(obj => obj.Name == item.Name);
        }

        public static void Remove(CartItemDataModel item)
        {
            CartDataModel.Items.Remove(item);
        }

        public static bool IsEmpty()
        {
            return items.Count == 0;
        }

        public static bool Checkout(string fullName, string address, string telephone)
        {
            string url = "make_order/?";

            foreach (var item in items)
            {
                url += String.Format("product={0}&quantity={1}&", item.Pk, item.Quantity.ToString());
            }
            url += String.Format("full_name={0}&address={1}&telephone={2}", fullName, address, telephone);

            Debug.WriteLine(url);

            try
            {
                var request = Client.Get(url, 15);
                request.Wait();

                var response = request.Result;
      
                if (response == "true")
                {
                    // TODO: Action in case of successfull order.
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                var x = e;

                return false;
            }
        }

        public int Sum
        {
            get
            {
                sum = 0;
                foreach (var item in Items)
                {
                    sum += item.Sum;
                }

                return sum;
            }

            set
            {
                sum = value;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Sum"));
                    //PropertyChanged(this, new PropertyChangedEventArgs("Items"));

                    PropertyChanged(this, new PropertyChangedEventArgs("SumString"));

                    // Work aroud for bindings updating.
                    var cartItemDataModel = new CartItemDataModel();
                    Items.Add(cartItemDataModel);
                    Items.Remove(cartItemDataModel);
                }
            }
        }

        public string SumString
        {
            get { return String.Format(AppResources.Sum, Sum); }
        }
    }
}
