﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UPlatform.Resources;

namespace UPlatform
{
    public class CartItemDataModel : ProductDataModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int quantity;

        public CartItemDataModel()
        {

        }

        public CartItemDataModel(ProductDataModel productDataModel)
        {
            this.Pk = productDataModel.Pk;
            this.Name = productDataModel.Name;
            this.Cost = productDataModel.Cost;
            this.Description = productDataModel.Description;
            this.Image = productDataModel.ImageNative;

            this.Quantity = 1;

            // TODO: Remove casting.
            this.Sum = (int)this.Cost * this.Quantity;
        }

        public int Quantity
        { 
            get { return quantity; }
            set
            {
                quantity = value;

                //TODO: Remove casting.
                this.Sum = (int)this.Cost * this.Quantity;

                if (PropertyChanged != null)
                {
                    // For update data bindings.
                    PropertyChanged(this, new PropertyChangedEventArgs("QuantityString"));
                    PropertyChanged(this, new PropertyChangedEventArgs("SumString"));
                    PropertyChanged(this, new PropertyChangedEventArgs("DetailString"));
                }
            }
        }

        public string DetailString
        {
            get { return String.Format("{0} x {1} = {2}", Cost, Quantity, Sum); }
        }

        public int Sum { get; set; }

        public string QuantityString
        {
            get { return String.Format(AppResources.Quantity, Quantity); }
        }

        public string SumString
        {
            get { return String.Format(AppResources.Sum, Sum); }
        }
    }
}
