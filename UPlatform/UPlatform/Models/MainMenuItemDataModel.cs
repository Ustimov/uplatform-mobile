﻿/*
 *  This file is part of UPlatform Project.
 *
 *    Copyright (c) Ustimov Artem, 2015.
 *    
 *          http://ustimov.org/
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UPlatform.Resources;

namespace UPlatform
{
    public class MainMenuItemDataModel
    {
        public static ObservableCollection<MainMenuItemDataModel> Demo()
        {
            return new ObservableCollection<MainMenuItemDataModel>
            {
                new MainMenuItemDataModel { Name = AppResources.Cart,
                    Image = "ic_action_action_cart", PageEntity = PageEntity.CART },
                new MainMenuItemDataModel { Name = AppResources.Contacts,
                    Image = "ic_action_contacts", PageEntity = PageEntity.CONTACTS },
                new MainMenuItemDataModel { Name = AppResources.AboutUs,
                    Image = "ic_action_action_about_us", PageEntity = PageEntity.ABOUT_US },
                new MainMenuItemDataModel { Name = AppResources.AboutProgram,
                    Image = "ic_action_action_settings", PageEntity = PageEntity.ABOUT_PROGRAM }
            };
        }

        public string Name { get; set; }

        public string Image { get; set; }

        public PageEntity PageEntity { get; set; }

    }
}
