﻿using Microsoft.Phone.Controls;

using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;

using UPlatform;
using UPlatform.WinPhone;

[assembly: ExportRenderer(typeof(AboutUsPage), typeof(HideApplicationBarPageRenderer))]
[assembly: ExportRenderer(typeof(CartPage), typeof(HideApplicationBarPageRenderer))]
[assembly: ExportRenderer(typeof(CheckoutPage), typeof(HideApplicationBarPageRenderer))]
[assembly: ExportRenderer(typeof(ContactsPage), typeof(HideApplicationBarPageRenderer))]
[assembly: ExportRenderer(typeof(ProductDetailPage), typeof(HideApplicationBarPageRenderer))]

namespace UPlatform.WinPhone
{
    class HideApplicationBarPageRenderer : PageRenderer
    {
        protected override void UpdateNativeWidget()
        {
            base.UpdateNativeWidget();

            var page = (PhoneApplicationPage)((PhoneApplicationFrame)System.Windows.Application.Current.RootVisual).Content;
            page.ApplicationBar.IsVisible = false;
        }
    }
}
