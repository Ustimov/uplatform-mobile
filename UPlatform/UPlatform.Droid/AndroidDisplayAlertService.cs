﻿using System;
using System.Diagnostics;

using Android.App;
using Android.Widget;
using Android.Content.Res;

using Xamarin.Forms;

using UPlatform.Droid;

[assembly: DependencyAttribute(typeof(AndroidDisplayAlertService))]

namespace UPlatform.Droid
{
    public class AndroidDisplayAlertService : IDisplayAlertService
    {
        public void ShowAlert(string title, string content, string buttonText)
        {
            var alert = new AlertDialog.Builder(Forms.Context);
            alert.SetTitle(title);
            alert.SetMessage(content);
            alert.SetNeutralButton(buttonText, (sender, e) => { });

            var dialog = alert.Show();
            BrandAlertDialog(dialog);

        }

        private static void BrandAlertDialog(AlertDialog dialog)
        {
            try
            {
                Android.Content.Res.Resources resources = dialog.Context.Resources;

                var alertTitleId = resources.GetIdentifier("alertTitle", "id", "android");

                var alertTitle = (TextView)dialog.Window.DecorView.FindViewById(alertTitleId);
                alertTitle.SetTextColor(Android.Graphics.Color.Black);

                var titleDividerId = resources.GetIdentifier("titleDivider", "id", "android");

                var titleDivider = dialog.Window.DecorView.FindViewById(titleDividerId);
                titleDivider.SetBackgroundColor(Android.Graphics.Color.Black);
            }
            catch(Exception e)
            {
                Debug.WriteLine("Android display alert service exception");
            }
        }
    }
}

