﻿using System;

using Android.App;
using Android.Widget;
//using Android.Views;

using UPlatform;
using UPlatform.Droid;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomDialog), typeof(CustomDialogRenderer))]

namespace UPlatform.Droid
{
    public class CustomDialogRenderer : ViewRenderer<CustomDialog, LinearLayout>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CustomDialog> e)
        {
            base.OnElementChanged(e);


            e.NewElement.OnShow += (sender, arg) => 
                {
                    var activity = this.Context as Activity;

                    AlertDialog alertDialog = new AlertDialog.Builder(activity).Create();

                    var parent = Parent as Android.Views.ViewGroup;

                    if (parent != null)
                    {
                        parent.RemoveView(this);
                    }

                    alertDialog.SetView(this);



                    alertDialog.Show();
                };

        }
    }
}

